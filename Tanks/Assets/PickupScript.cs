﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PickupScript : MonoBehaviour
{

    public TextMeshProUGUI CurrencyCounter;
    public TextMeshProUGUI CurrencyCounter2;
    public GameObject Pickup;
    GameManager GM;
    // Start is called before the first frame update
    void Start()
    {
        GameObject GMobj = GameObject.FindGameObjectWithTag("GameController");
        GM = GMobj.GetComponent<GameManager>();

    }

    void OnTriggerEnter(Collider other)
    {
        if (GetComponent<MeshRenderer>() == null)
            {
            return;
        }
        if (!other.gameObject.CompareTag("Player")) { return; }
       int PlayerID = other.gameObject.GetComponent<TankMovement>().m_PlayerNumber;
        GM.count[PlayerID-1] = GM.count[PlayerID-1] + 1;
        SetCountText();
        StartCoroutine(PickupRespawn());
        GetComponent<MeshRenderer>().enabled= false;
        GetComponent<Collider>().enabled = false;
       
    }

    IEnumerator PickupRespawn()
    {
        yield return new WaitForSeconds(5);
        GetComponent<MeshRenderer>().enabled = true;
        GetComponent<Collider>().enabled = true;

    }


    void SetCountText()
    {
        CurrencyCounter.text = "$$$: " + GM.count[0].ToString();
        CurrencyCounter2.text = "$$$: " + GM.count[1].ToString();
    }
}

